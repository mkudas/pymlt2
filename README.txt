## Install Anaconda
Download Anaconda

> ./Anaconda.sh
> conda update conda
> conda update anaconda


## Install opencv

> conda install -c https://conda.binstar.org/menpo opencv

# check
>> import cv2


## Install MySQLdb

> easy_install mysql-python

# check:
> python
>> import MySQLdb


## Install pyrax @ github.com/rackspace/pyrax
## This gives you the python interface to rackspace

> pip install pyrax

check:: python
>> import pyrax

## Install rackspace vpn

to install vpnc
> sudo apt-get install network-manager-vpnc

to setup vpnc
in linux, open settings -> network connections
button -> +add -> cisco vpn

98.129.243.137
username: <username>
password: <password>
groupname: rack-819222
groupnamepassword: <password>


## Install pymlt2

git clone git@bitbucket.com:mkudas/pymlt2.git