import pickle
import random
import os
from pautils import db as padb

LOG_DIRECTORY = './log'

class model_classification():
	def __init__(self,site,mindate,maxdate,baygroup=None,n_samples = 10000):
		self.baygroup = baygroup
		self.mindate = mindate
		self.maxdate = maxdate
		self.site = site
		self.n_samples = n_samples
		self.bayentries = {}

	## DUMP DATA
	def download_images(self):
		entries = self.return_modeldata(self.site,self.mindate,self.maxdate)
		padb.download_images(entries[0])

	## UTILS 
	def create_directory(self,directory):
		if not os.path.exists(directory):
			os.makedirs(directory)

	## CREATE ENTRIES
	def return_mac_from_filename(self,filename):
		return filename.split('/')[3].replace('.jpg','')

	def filter_entries(self,baylist,entries):
		"""get all the entries, shuffle them, select only ones in the baylist, and return the top n_samples"""
		fil = entries[0]
		res = entries[1]
		vec = entries[2]
		filtered_entries =  [(fil,res,vec) for fil,res,vec in zip(fil,res,vec) if self.return_mac_from_filename(fil) in baylist]
		return [[x[0] for x in filtered_entries[0:self.n_samples]],[x[1] for x in filtered_entries[0:self.n_samples]],[x[2] for x in filtered_entries[0:self.n_samples]]]

	def return_modeldata(self,site,min_date,max_date):
		"""get all the entries, shuffle them, and return the top n_samples"""
		filepaths,results,vectors = padb.return_modeldata(site,min_date,max_date)
		return (filepaths,results,vectors)

	def return_snapshot(self,site,date,time):
		filepaths,results,vectors = padb.return_snapshot(site,date,time)
		return (filepaths,results,vectors)

	def create_entries(self):
		random.seed(self.seed)
		entries = self.return_modeldata(self.site,self.mindate,self.maxdate)
		entr = [(f,r,v) for f,r,v in zip(entries[0],entries[1],entries[2])]
		random.shuffle(entr)
		shuffed = ([e[0] for e in entr], [e[1] for e in entr], [e[2] for e in entr])
		if self.baygroup:
			for baypool in self.baygroup.baygroups.keys():
				print "create_entries :: baypool", baypool
				self.bayentries[baypool] = self.filter_entries(self.baygroup.baygroups[baypool], shuffed)
				print "create_entries :: total entries", len(self.bayentries[baypool][0])

		self.entries = ([e[0] for e in entr[0:self.n_samples]], [e[1] for e in entr[0:self.n_samples]], [e[2] for e in entr[0:self.n_samples]])


	## PREPROCESS IMAGES INTO FEATURE VECTORS IF NECESSARY
	def preprocess(self):
		if self.baygroup:
			for baypool in self.baygroup.baygroups.keys():
				print "preprocessing :: baypool", baypool
				print "preprocessing :: total entries", len(self.entries[0]), type(self.entries[0])
				if self.entries:
					entries = self.bayentries[baypool]
					self.bayentries[baypool][1] = entries[1]
					self.bayentries[baypool][2] = self.format_vectors_from_filenames(entries[0])

					print "preprocessing :: filtered vectors : {0}".format(len(self.bayentries[baypool][2]))
		elif self.entries:
			self.results = self.entries[1]
			self.vectors = self.return_vectors_from_filenames(self.entries[0])
			print "preprocessing :: vectors : {0}".format(len(self.vectors))

	## BUILD MODEL
	def build(self):
		if self.baygroup:
			self.model_dict = {}
			for baypool in self.baygroup.baygroups.keys():
				print "building :: baypool", baypool
				print "building :: total entries", len(self.entries[0])
				if len(self.baygroup.baygroups[baypool][2]):
					vectors = self.bayentries[baypool][2]
					results = self.bayentries[baypool][1]
					model = self.init_model()
					self.train_model(model,vectors,results)
					self.model_dict[baypool] = model
		else:
			if self.entries:
				print "building :: single model:: vectors : {0}".format(len(self.vectors))
				self.model = self.init_model()
				self.train_model(self.model, self.vectors,self.results)
		return self.model

	def format_vector_from_filename(self,filename):
		print "This is virtual :: format_vector_from_filename has not been defined"

	def init_model(self):
		print "This is virtual :: init_model has not been defined"

	## BUILD MODEL
	def regress(self,snapshot):
		regression_results = {'correct':0, 'total': 0, 'unpredicted': 0, 'missed vehicle':0, 'missed bay':0}
		regression_results['total'] = snapshot.frame_count()
		correct = 0 ; total = 0

		vectors = snapshot.return_vectors()
		filenames = snapshot.return_filepaths()
		results = snapshot.return_results()

		print "regress :: snapshot size : {0}".format(len(vectors))
		print "regress :: date {0}".format(snapshot.datekey)
		print "regress :: time {0}".format(snapshot.timekey)
		print "regress :: snapshot vehicles : {0}".format(len([res for res in results if res == 1]))
		print "regress :: snapshot bays     : {0}".format(len([res for res in results if res == 0]))

		group_regression_result = {}
		if self.baygroup:
			#init group regression
			for baypool in self.baygroup.baygroups.keys():
				group_regression_result[baypool]  = {'correct':0, 'total': 0, 'unpredicted': 0, 'missed vehicle':0, 'missed bay':0}

			for vec in zip(results,vectors,filenames):
				baygrouping = None
				
				for baypool in self.baygroup.baygroups.keys():
					if self.return_mac_from_filename(vec[2]) in self.baygroup.baygroups[baypool]:
						baygrouping = baypool
						break

				if baygrouping:
					test_feature_vector = self.return_vectors_from_filenames([vec[2]])[0]
					prediction = self.predict(self.model_dict[baygrouping],test_feature_vector)

					group_regression_result[baygrouping]['total'] += 1
					if int(prediction) == int(vec[0]):
						regression_results['correct'] += 1
						group_regression_result[baygrouping]['correct'] += 1
					elif int(prediction) == 1:
						regression_results['missed vehicle'] += 1
						group_regression_result[baygrouping]['missed vehicle'] += 1
					else:
						regression_results['missed bay'] += 1
						group_regression_result[baygrouping]['missed bay'] += 1

				else:
					print "regress :: {0} not found in baygrouping".format(self.return_mac_from_filename(vec[2]))
					regression_results['unpredicted'] += 1
					for baypool in self.baygroup.baygroups.keys():
							print baypool, " :: ", group_regression_result[baypool]


		else:			
			vectors = self.return_vectors_from_filenames(filenames)
			for vec in zip(results,vectors):
				prediction = self.predict(vec[1])
				if prediction == int(vec[0]):
					regression_results['correct'] += 1					
				elif int(prediction) == 1:
					regression_results['missed vehicle'] += 1					
				else:
					regression_results['missed bay'] += 1
					
		print regression_results
		return regression_results

