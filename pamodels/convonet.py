import sys,getopt,os
import numpy as np
import random
import skimage

import pautils.db as padb
import pautils.image as paimage
import pamodels.classification as mc

convopath = ["home/parkassist/Projects/2014DeepLearning/neural-networks-and-deep-learning/code/old","home/parkassist/Projects/2014DeepLearning/neural-networks-and-deep-learning/code"]
for path in convopath:
	if path not in sys.path:
		sys.path.append(path)

class model_convonet(mc.model_classification):
	def __init__(self,site,mindate,maxdate, baygroup = None,n_samples = 50000,n_validation = 10000):
		self.site = site
		self.entries = None
		self.numentries = 10000
		self.seed = 10000
		self.model = None
		self.mindate = mindate
		self.maxdate = maxdate
		self.baygroup = baygroup
		self.n_samples = n_samples
		self.model_dict = {}
		self.vectors = []

	def filter_entries(self,baylist):
		fil = self.entries[0]
		res = self.entries[1]
		vec = self.entries[2]
		filtered_entries =  [(fil,res,vec) for fil,res,vec in zip(fil,res,vec) if self.return_mac_from_filename(fil) in baylist]
		return ([x[0] for x in filtered_entries[0:self.n_samples]],[x[1] for x in filtered_entries[0:self.n_samples]],[x[2] for x in filtered_entries[0:self.n_samples]])

	def predict(self,vector):
		return self.model.predict(vector)

	def create_mnist_package(self):
		filename = "pa_cnn_training.pkl"
		train_set,valid_set = self.create_vector_package()
		import cPickle
		import gzip

		#dump it
		cPickle.dump((train_set,valid_set),open(filename,'w'))

		#zip it
		f_in = open(filename,'rb')
		f_out = gzip.open(filename + '.gz','wb')
		f_out.writelines(f_in)
		f_out.close()
		f_in.close()

	def create_vector_package(self):
		if self.entries:
			# create training data
			train_groundtruth = np.asarray(self.entries[1][0:1000],dtype=np.int64)
			train_data = np.asarray(self.return_vectors_from_filenames(self.entries[0][0:1000]),dtype=np.int64)

			# create test data
			test_groundtruth = np.asarray(self.entries[1][1000:1100],dtype=np.int64)
			test_data = np.asarray(self.return_vectors_from_filenames(self.entries[0][1000:1100]),dtype=np.int64)

			# create validation data
			#valid_rec = np.asarray(self.entries[1][0:self.n_samples])
			#valid_data = np.asarray(self.return_vectors_from_filenames(self.entries[0][0:self.n_samples]))

		return((train_data,test_groundtruth),(test_data, test_groundtruth))


	def return_vectors_from_filenames(self,filenames):
		vectors = []
		padb.download_images(filenames)
		for filename in filenames:
			image = padb.return_image(filename)
			filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))
			out = paimage.return_affine(filepath)
			im_resize = skimage.transform.resize(out,(100,100))
			im_resize = im_resize.astype(np.float64)
			vectors.append( np.reshape(im_resize, 10000) )
		return vectors
	
	
	def get_filenames(self,filename):
		with open(filename, 'r') as fh:
			data = fh.readlines()
		line     = [d.strip() for d in data[1:]]
		datalist = [d.split(',') for d in line]
		filenames = [d[1] for d in datalist]
		results  = [int(d[2]) for d in datalist]
		return filenames, results

	def preprocess(self):
		#pathname = os.path.join(inputpath,'imagedb_convoultion/')
		pathname = './imagedb_convoultion/'
		self.create_directory(pathname)
		filenames = self.entries[0]
		padb.download_images(filenames)

		for name in filenames:
			fname = name.replace('/','_')
			filepath = os.path.join(padb.image_db_location,fname)
			out = paimage.return_affine(filepath)
			
			im_resize = skimage.transform.resize(out,(100,100))
			im_resize = im_resize.astype(np.float64)
			self.vectors.append( np.reshape(im_resize, 10000) )

			#save in case
			fnameout = fname.replace(".jpg","_polycut.jpg")
			skimage.io.imsave(os.path.join(pathname,fnameout),out)

	def return_convoed_snapshot(self,site,date,time):
		filenames,results,_ = self.return_snapshot(site,date,time)
		pathname = './imagedb_convoultion/'
		#self.create_directory(pathname)
		#padb.download_images(filenames)
		vectors = []

		for name in filenames:
			fname = name.replace('/','_')
			filepath = os.path.join(padb.image_db_location,fname)
			out = paimage.return_affine(filepath)
			
			im_resize = skimage.transform.resize(out,(100,100))
			im_resize = im_resize.astype(dtype=np.int64)

			vectors.append( np.reshape(im_resize, 10000) )

			#save in case
			fnameout = fname.replace(".jpg","_polycut.jpg")
			skimage.io.imsave(os.path.join(pathname,fnameout),out)
		'''
		filename = "pa_cnn_validation.pkl"
		import cPickle
		import gzip

		#dump it
		cPickle.dump((np.asarray(vectors),np.asarray(results,dtype=np.int64)),open(filename,'w'))

		#zip it
		f_in = open(filename,'rb')
		f_out = gzip.open(filename + '.gz','wb')
		f_out.writelines(f_in)
		f_out.close()
		f_in.close()
		'''
		return(vectors,results)

class model_convo_writer(model_convonet):
	def __init__(self,site,mindate,maxdate, baygroup = None,n_samples = 50000,n_validation = 10000):
		self.site = site
		self.entries = None
		self.numentries = 10000
		self.seed = 10000
		self.model = None
		self.mindate = mindate
		self.maxdate = maxdate
		self.baygroup = baygroup
		self.n_samples = n_samples
		self.model_dict = {}
		self.vectors = []

	def preprocess(self):
		#pathname = os.path.join(inputpath,'imagedb_convoultion/')
		pathname = './imagedb_convoultion/'
		self.create_directory(pathname)
		filenames = self.entries[0]
		labels = self.entries[1]
		print filenames
		padb.download_images(filenames)
		print "downloading"
		for i,name in enumerate(filenames):
			fname = name.replace('/','_')
			filepath = os.path.join(padb.image_db_location,fname)

			out = paimage.return_affine(filepath, gray = False)
			
			#im_resize = skimage.transform.resize(out,(100,100))
			#im_resize = im_resize.astype(np.float64)
			#self.vectors.append( np.reshape(im_resize, 10000) )

			#save in case
			fnameout = fname.replace(".jpg","_polycut.jpg")
			if labels[i]:
				destination = '/media/Hard Disk/2014Deep_Learning_Data/nn_data/pos'
				f = os.path.join(destination,fnameout)
				if not os.path.exists(f):
					skimage.io.imsave(f,out)
			else:
				destination = '/media/Hard Disk/2014Deep_Learning_Data/nn_data/neg'
				f = os.path.join(destination,fnameout)
				if not os.path.exists(f):
					skimage.io.imsave(f,out)
			
if __name__ == "__main__":
	try: 
		opts,args = getopt.getopt(sys.argv,"hd:d",["dump","build","regress"])
	except getopt.GetoptError:
		sys.exit(2)

			

