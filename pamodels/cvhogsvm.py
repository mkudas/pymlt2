import cv2
import numpy as np
import os
import skimage

import pamodels.classification as mc
import pautils.snapshot as ss
import pautils.db as padb
import pautils.image as paimage


class opencvhog(mc.model_classification):

	def __init__(self,site,mindate,maxdate, baygroup = None,n_samples = 10000):
		self.site = site
		self.entries = None
		self.numentries = 10000
		self.seed = 10000
		self.model = None
		self.mindate = mindate
		self.maxdate = maxdate
		self.baygroup = baygroup
		self.n_samples = n_samples
		self.hogParams = {'winStride':(8,8), 'padding':(32,32), 'scale': 1.05}
		self.scaleinfo = []
		self.model_dict = {}
		mc.model_classification.__init__(self,site,mindate,maxdate, baygroup,n_samples)

	def return_hog(self,filename):
		image = padb.return_image(filename)
		filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))
		out = paimage.return_warp(filepath)
		hog = cv2.HOGDescriptor((100,100), (50,50), (25,25), (25,25),9)
		hist = hog.compute(cv2.convertScaleAbs(out))
		return hist

	def format_vector_from_filename(self,filename):
		print "cvhogsvm :: formating snapshot vector"
		padb.download_images([filename])
		image = padb.return_image(filename)
		filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))
		out = paimage.return_warp(filepath)
		hog = cv2.HOGDescriptor((100,100), (50,50), (25,25), (25,25),9)
		hist = hog.compute(cv2.convertScaleAbs(out))
		return hist

	def return_vectors_from_filenames(self,filenames):
		print "cvhogsvm :: format_vectors"
		vectors = []
		padb.download_images(filenames)

		for filename in filenames:
			image = padb.return_image(filename)
			filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))

			# create hog vector
			out = paimage.return_warp(filepath)
			hog = cv2.HOGDescriptor((100,100), (50,50), (25,25), (25,25),9)
			vec = hog.compute(cv2.convertScaleAbs(out))
			print vec
			vec = [v[0] for v in vec]
			v = np.asarray(vec,dtype=np.float32 )
			print type(v), v.size,v.shape,v.dtype
			print v
			vectors.append(v )
		#vectors = np.asarray(vectors)
		return vectors 

	def init_model(self):
		print "cvhogsvm :: init model"
		self.model = cv2.SVM()
		return self.model

	def train_model(self,model,vectors,results):		
		print "cvhogsvm :: training model"		
		print type(self.model)
		svm_params = dict( kernel_type = cv2.SVM_RBF,
            svm_type = cv2.SVM_C_SVC,
            #C=2.67, 
            #gamma=5.383,
            nu = 0.2,
            term_crit = (cv2.TERM_CRITERIA_MAX_ITER, 1000, 1e-6))
		v = np.asarray(vectors, dtype=np.float32)
		r = np.asarray(results,dtype=np.int32)
		print type(v),v.shape,v.size,v.dtype
		print type(r),r.shape,r.size,r.dtype
		print type(v[0]),v[0].shape,v[0].size,v.dtype
		print v[0]

		self.model.train(v,r,params = svm_params)

	def predict(self,model,vector):
		print "cvhogsvm :: predict model"
		return model.predict(np.asarray(vector))


class opencvhogwithfeature(opencvhog):

	def calcscaling(self,vectors):		
		minval = []; maxval = [] ; feature = []
		
		for i in range(len(vectors[0])):
			feature.append([x[i] for x in vectors])
		for i in range(len(vectors[0])):
			minval.append(min(feature[i]))
			maxval.append(max(feature[i]))
	
		self.scaleinfo = [(x[1],1/(x[0] - x[1])) for x in zip(maxval,minval)]

	def train_model(self,model,vectors,results):				
		model.train(np.asarray(vectors),np.asarray(results))

	def scaleFeatures(self,featurevector):
		scaledvec = []
		for fv in featurevector:
			scaledvec.append([float(f-s[1]) * s[0]  for f,s in zip(fv,self.scaleinfo)])
		print "scale vec lent 10 62",len(scaledvec),len(scaledvec[0])
		return scaledvec

	def return_vectors_from_filenames(self,filenames):
		vectors = []
		padb.download_images(filenames)
		rtvectors = []
		self.vectors = self.entries[2]

		for i,filename in enumerate(filenames):
			image = padb.return_image(filename)
			filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))
			# create hog vector
			out = paimage.return_warp(filepath)
			hog = cv2.HOGDescriptor((100,100), (50,50), (25,25), (25,25),9)
			vec = hog.compute(cv2.convertScaleAbs(out))			
			vectors.append( vec )
			# create 62 vector
			rtvectors.append(self.vectors[i])
		print "hog length", len(vectors[0])
		#scale 62 vector
		self.calcscaling(rtvectors)
		print "rt", type(self.scaleFeatures(rtvectors)),len(self.scaleFeatures(rtvectors)), len(self.scaleFeatures(rtvectors))
		print "hog", type(vectors),len(vectors),len(vectors[0])
		outvectors = []
		for x in zip(vectors,self.scaleFeatures(rtvectors)):
			newvec = x[0]
			for i in range(len(x[1])):
				newvec.append(x[1][i])
			outvectors.append(x)

		#outvectors = [ x[0] + x[1] for x in zip(vectors,self.scaleFeatures(rtvectors))]
		print "out vectors", type(outvectors),len(outvectors),len(outvectors[0])
		self.vectors = np.asarray( outvectors )
		return outvectors 

	def format_vectors_from_filenames(self,filenames):
		
		vectors = []
		padb.download_images(filenames)

		for filename in filenames:
			image = padb.return_image(filename)
			filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))
			# create hog vector
			out = paimage.return_warp(filepath)
			hog = cv2.HOGDescriptor((100,100), (50,50), (25,25), (25,25),9)
			vec = hog.compute(cv2.convertScaleAbs(out))
			vectors.append( vec )
			rtvectors.append(self.vectors[i])
		
		self.calcscaling(rtvectors)
		vectors = [hog + rt for hog,rt in zip(vectors,self.scaleFeatures(rtvectors))]
		vectors = np.asarray( vectors )
		self.vectors = vectors
		return vectors 

class opencvhog_zz(opencvhog):

	def calcscaling(self,vectors):		
		minval = []; maxval = [] ; feature = []
		
		for i in range(len(vectors[0])):
			feature.append([x[i] for x in vectors])
		for i in range(len(vectors[0])):
			minval.append(min(feature[i]))
			maxval.append(max(feature[i]))

		self.scaleinfo = (minval, [1/(mx - mn) for mx,mn in zip(maxval,minval)])
		return minval,[1/(mx - mn) for mx,mn in zip(maxval,minval)]

	def scalevector(self,vector):
		scaled_vector = [(v-vmin) * scalefactor for v,vmin,scalefactor in zip(vector,self.scaleinfo[0],self.scaleinfo[1])]
		return scaled_vector
	
	def return_vector_from_filename(self,filename):
		
		out = paimage.return_warp(filename)
		params = dict(
			_winSize = (100,100),
			_blockSize = (50,50),
			_blockStride = (25,25),
			_cellSize = (25,25),
			_nbins = 9,
			_derivAperture = 1,
			_winSigma = -1,
			_histogramNormType = 0L,
			#_L2HysThreshold = 0.2,
			_gammaCorrection = True,
			_nlevels = 1)
		hog = cv2.HOGDescriptor(**params)
		vec = hog.compute(out)
		return vec

	def create_entries(self):
		print 'im here'
		positive_directory = ['/media/parkassist/Hard Disk/brisban/test_low/im_3_6/positive',
		'/media/parkassist/Hard Disk/brisban/test_low/im_3_7/positive',
		'/media/parkassist/Hard Disk/brisban/test_low/im_3_10/positive']
		negative_directory = ['/media/parkassist/Hard Disk/brisban/test_low/im_3_6/negative',
		'/media/parkassist/Hard Disk/brisban/test_low/im_3_7/negative',
		'/media/parkassist/Hard Disk/brisban/test_low/im_3_10/negative']

		results   = []
		vectors   = []
		filenames = []
		rtvectors = []
		scaledvectors = []
		import os
		positive = 0 
		for pd in positive_directory:
			for root,directory,files in os.walk(pd):
				for f in files:
					try:
						if positive < 955:
							positive += 1
							rtvector = paimage.return_rt_string(os.path.join(root,f))
							rtvectors.append(rtvector)
							hogvector = self.return_vector_from_filename(os.path.join(root,f))
							vectors.append(hogvector)
							results.append(1)
							filenames.append(os.path.join(root,f))
					except:
						pass
	
		negative = 0
		for nd in negative_directory:
			for root,directory,files in os.walk(nd):
				for f in files:
					try:
						negative += 1
						rtvector = paimage.return_rt_string(os.path.join(root,f))
						rtvectors.append(rtvector)
						hogvector = self.return_vector_from_filename(os.path.join(root,f))
						vectors.append(hogvector)
						results.append(0)
						filenames.append(os.path.join(root,f))
					except:
						pass
		print "len", negative
		# calculate scaling
		(vmin,scale) = self.calcscaling(rtvectors)

		for hogvector,rtvector in zip(vectors,rtvectors):
			scaled_rtvector = np.swapaxes(np.array([self.scalevector(rtvector)]),0,1)
			newhogvector = np.concatenate((hogvector,scaled_rtvector), axis=0)
			scaledvectors.append(newhogvector.astype(np.float32))
		
		self.vectors = scaledvectors
		self.results = results
		self.rtvectors = rtvectors

		print "length of data: vectors {0} results {1}".format(len(self.vectors), len(self.results))
		
		#entr = [(f,r,v) for f,r,v in zip(filenames,results,vectors)]
		#random.shuffle(entr)
		#shuffed = ([e[0] for e in entr], [e[1] for e in entr], [e[2] for e in entr])
		#self.entries = ([e[0] for e in entr[0:self.n_samples]], [e[1] for e in entr[0:self.n_samples]], [e[2] for e in entr[0:self.n_samples]])

	def regress(self):
		regression_results = {'correct':0, 'total': 0, 'unpredicted': 0, 'missed vehicle':0, 'missed bay':0, 'totalveh':0, 'totalbay':0}
		rt_regression_results = {'correct':0, 'total': 0, 'unpredicted': 0, 'missed vehicle':0, 'missed bay':0, 'totalveh':0, 'totalbay':0}
		
		negative_directory = ['/media/parkassist/Hard Disk/brisban/test_low/im_3_14/rtnegative/negative',
			'/media/parkassist/Hard Disk/brisban/test_low/im_3_14/rtpositive/negative']
		positive_directory = ['/media/parkassist/Hard Disk/brisban/test_low/im_3_14/rtpositive/positive',
			'/media/parkassist/Hard Disk/brisban/test_low/im_3_14/rtnegative/positive']
		results   = []
		vectors   = []
		filenames = []
		rtvectors = []
		scaledvectors = []
		import os
		fail_count = 0
		success_count = 0
		for pd in positive_directory:
			for root,directory,files in os.walk(pd):
				for f in files:
					try:
						rtvector = paimage.return_rt_string(os.path.join(root,f))
						rtvectors.append(rtvector)
						hogvector = self.return_vector_from_filename(os.path.join(root,f))
						scaled_rtvector = np.swapaxes(np.array([self.scalevector(rtvector)]),0,1)
						newhogvector = np.concatenate((hogvector,scaled_rtvector), axis=0)
						scaledvectors.append(newhogvector.astype(np.float32))
						results.append(1)
						filenames.append(os.path.join(root,f))
						success_count += 1
					except:
						print "FAIL", os.path.join(root,f)
						fail_count += 1
						pass
						
		for nd in negative_directory:
			for root,directory,files in os.walk(nd):
				for f in files:
					try:
						rtvector = paimage.return_rt_string(os.path.join(root,f))
						rtvectors.append(rtvector)
						scaled_rtvector = np.swapaxes(np.array([self.scalevector(rtvector)],float),0,1)
						hogvector = self.return_vector_from_filename(os.path.join(root,f))
						newhogvector = np.concatenate((hogvector,scaled_rtvector), axis=0)
						scaledvectors.append(newhogvector.astype(np.float32))
						results.append(0)
						filenames.append(os.path.join(root,f))
						success_count += 1
					except:
						fail_count +=1
						print "FAIL", os.path.join(root,f)
						pass
		
		for result,vector,rtvector in zip(results,scaledvectors,rtvectors):
			regression_results['total'] += 1
			prediction = int(self.predict(self.model,vector))
			rtprediction =  int(self.rtmodel.predict(rtvector))
	
			if result == 0:
				regression_results['totalbay'] += 1
			if result == 1:
				regression_results['totalveh'] += 1
			
			if prediction == result:
				regression_results['correct'] += 1					
			elif result == 1:
				regression_results['missed vehicle'] += 1					
			else:
				regression_results['missed bay'] += 1

			if rtprediction == result:
				rt_regression_results['correct'] += 1					
			elif result == 1:
				rt_regression_results['missed vehicle'] += 1					
			else:
				rt_regression_results['missed bay'] += 1
		print regression_results
		print rt_regression_results

'''
if __name__ == "__main__":
	

	# create standard model, print results
	test_snapshot_list = [('20141018','1500')]
	results_list = []
	print "init model"
	cvhog = opencvhog(site = 2, maxdate = 20141018, mindate = 20141001)
	print "creating..."
	cvhog.create_entries()
	print "building..."
	cvhog.build()
	for snapshot in test_snapshot_list:
		snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
		print "regressing..."
		results_list.append(rawsvm.regress(snap))
	for result in results_list:
		rawsvm.print_regression(result)
'''