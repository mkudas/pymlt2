import sys,getopt,os
import random
from sklearn.ensemble import RandomForestClassifier


import pamodels.classification as mc
import pautils.snapshot as ss
import pautils.db as padb
import pautils.image as paimage

import pautils.db as padb
import pautils.image as paimage



class model_randomtree(mc.model_classification):

	def __init__(self,site,mindate,maxdate, baygroup = None,n_samples = 10000):
		self.site = site
		self.entries = None
		self.numentries = n_samples
		self.seed = 10000
		self.model = None
		self.mindate = mindate
		self.maxdate = maxdate
		self.baygroup = baygroup
		self.model_dict = {}
		mc.model_classification.__init__(self,site,mindate,maxdate, baygroup,n_samples)
	
	def predict(self,vector):
		prediction = 0
		if self.model.predict(vector) > 0.5:
			prediction = 1
		return prediction

	def train_model(self,model,vectors,results):
		self.model = RandomForestClassifier(n_estimators = 50, max_depth = 50, max_features = 7)		
		self.model.fit(vectors,results)
		print "training model :: Random Tree"
		return self.model

class model_saltedtree(model_randomtree):

	def filter_salt(self,salt,baylist):
		fil = salt[0]
		res = salt[1]
		vec = salt[2]
		filtered_entries =  [(fil,res,vec) for fil,res,vec in zip(fil,res,vec) if self.return_mac_from_filename(fil) in baylist]
		return ([x[0] for x in filtered_entries],[x[1] for x in filtered_entries],[x[2] for x in filtered_entries])

	def build(self):
		if self.baygroup:
			salt_entries = self.return_salt(self.site,self.mindate,self.maxdate)
			self.model_dict = {}
			for baypool in self.baygroup.baygroups.keys():
				print "building :: baypool", baypool
				print "building :: total entries", len(self.entries[0])
				if self.entries:
					entries = self.filter_entries(self.baygroup.baygroups[baypool])
					salt    = self.filter_salt(salt_entries,self.baygroup.baygroups[baypool])
					print "building :: presalted filtered entries", len(entries[0])
					results = entries[1] + salt[1]
					vectors = entries[2] + salt[2]
					print "building :: postsalted filtered entries", len(vectors), len(salt)
					
					model = RandomForestClassifier(n_estimators = 50, max_depth = 50, max_features = 7)
					self.model_dict[baypool] = model.fit(vectors,results)
					
		else:
			print "not implimented"
		return self.model

	def return_salt(self,site,min_date,max_date):
		filepaths,results,vectors = padb.return_salt(site,min_date,max_date)
		return [filepaths,results,vectors]

class model_ibm(model_randomtree):

	def create_entries(self):
		random.seed(self.seed)
		entries = padb.return_baygroup_modeldata(self.site,self.mindate,self.maxdate,self.bay)
		print "ibm :: create entries :  {0}".format(len(entries))
		random.shuffle(entries)
		self.entries = entries[0:self.numentries]
		return self.entries

class model_mutliscale_rt(model_randomtree):

	def return_vectors_from_filenames(self,filenames):
		vectors = []
		for filename in filenames:
			image = padb.return_image(filename)
			filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))
			vector = paimage.return_cv_vectors(filepath)
			vectors.append( vector )
		return vectors 


if __name__ == "__main__":
	try: 
		opts,args = getopt.getopt(sys.argv,"hd:d",["dump","build","regress"])
	except getopt.GetoptError:
		sys.exit(2)
	# test standard model
	'''
	snap = ss.snapshot(site = 2, datekey = 20141018, timekey = 1500)
	print "init model"
	#standard_model = model_randomtree(site = 2, maxdate = 20141018, mindate = 20140721)
	standard_model = model_randomtree(site = 2, maxdate = 20141018, mindate = 20141008)
	print "creating..."
	standard_model.create_entries()
	print "building..."
	standard_model.build()
	print "regressing..."
	results_standard_model = standard_model.regress(snap)
	standard_model.print_regression(results_standard_model)
	
	# test ibm model
	snap = ss.snapshot(site = 2, datekey = 20141018, timekey = 1500)
	print "init model"
	bay = ['84-6e-b1-00-22-56-1']
	ibm_model = model_ibm(site = 2, maxdate = 20141018, mindate = 20140721,baygroup = bay)
	print "creating..."
	ibm_model.create_entries()
	print "building..."
	ibm_model.build()
	print "regressing..."
	results_ibm_model = ibm_model.regress(snap)
	ibm_model.print_regression(results_ibm_model)

	# test salted model
	snap = ss.snapshot(site = 2, datekey = 20141018, timekey = 1500)
	print "init model"
	salted_model = model_saltedtree(site = 2, maxdate = 20141018, mindate = 20140721)
	print "creating..."
	salted_model.create_entries()
	print "building..."
	salted_model.build()
	print "regressing..."
	results_salted_model = salted_model.regress(snap)
	salted_model.print_regression(results_salted_model)
	'''
	
