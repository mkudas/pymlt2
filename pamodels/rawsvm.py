import sys,getopt,os
import random
import numpy as np
import skimage
from skimage.transform import resize
from sklearn import svm

import pautils.db as padb
import pautils.snapshot as ss
import pautils.baygroup as baygroup
import pautils.image as paimage
from skimage.feature import hog
import pamodels.classification as mc

class model_rawsvm(mc.model_classification):
	def __init__(self,site,mindate,maxdate, baygroup = None,n_samples = 10000):
		self.site = site
		self.entries = None
		self.numentries = 10000
		self.seed = 10000
		self.model = None
		self.mindate = mindate
		self.maxdate = maxdate
		self.baygroup = baygroup
		self.n_samples = n_samples
		self.vectors = None
		self.results = None
		self.model_dict = {}

	def filter_entries(self,baylist):
		fil = self.entries[0]
		res = self.entries[1]
		vec = self.entries[2]
		filtered_entries =  [(fil,res,vec) for fil,res,vec in zip(fil,res,vec) if self.return_mac_from_filename(fil) in baylist]
		return ([x[0] for x in filtered_entries[0:self.n_samples]],[x[1] for x in filtered_entries[0:self.n_samples]],[x[2] for x in filtered_entries[0:self.n_samples]])

	def create_entries(self):
		random.seed(self.seed)
		entries = self.return_modeldata(self.site,self.mindate,self.maxdate)
		entr = [(f,r,v) for f,r,v in zip(entries[0],entries[1],entries[2])]
		random.shuffle(entr)
		self.entries = ([e[0] for e in entr], [e[1] for e in entr], [e[2] for e in entr])

		if self.baygroup and self.entries:
			print "create_entries :: baypool", baypool
			print "create_entries :: total entries", len(self.entries[0])
			results = {}
			vectors = {}
			for baypool in self.baygroup.baygroups.keys():
				entries = self.filter_entries(self.baygroup.baygroups[baypool])
				results[baypool] = entries[1]
				vectors[baypool] = self.return_vectors_from_filenames(entries[0])
		else:
			if self.entries:
				print "create_entries :: full site, no baypool"
				print "create_entries :: total entries", len(self.entries[0])
				results = self.entries[1][0:self.n_samples]
				vectors = self.return_vectors_from_filenames(self.entries[0][0:self.n_samples])
		self.results = results
		self.vectors = vectors
		return (results, vectors)

	def predict(self,vector):
		return self.model.predict(vector)

	def return_vectors_from_filenames(self,filenames):
		vectors = []
		padb.download_images(filenames)
		for filename in filenames:
			print "filename", filename
			image = padb.return_image(filename)
			filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))
			print filepath
			out = paimage.return_affine(filepath)
			#im = skimage.color.rgb2gray(image)
			im_resize = skimage.transform.resize(out,(28,28))
			im_resize = im_resize.astype(np.float64)
			vectors.append( np.reshape(im_resize, 784) )
		return vectors

	def load_results(self,results):

		self.vectors = results[1]
		self.results = results[0]
		for vec in self.vectors:
			print vec
		print "vector length", len(self.vectors)
		print "results length", len(self.results)


	def build(self):
		if self.baygroup:
			self.model_dict = {}
			for baypool in self.baygroup.baygroups.keys():
				print "building :: baypool", baypool
				if self.entries:
					print "building :: filtered vectors : {0}".format(len(self.vectors[baypool]))
					model = svm.SVC()
					self.model_dict[baypool] = model.fit(self.vectors[baygroup],self.results[baygroup])
		else:	
			print "building :: vectors : {0}".format(len(self.vectors))
			self.model = svm.SVC(gamma=0.0001, C=100.)
			
			
			self.model.fit(self.vectors,np.array(self.results))
		return self.model

	def test_regress(self):
		predictions = []
		for vec in zip(self.results,self.vectors):
			prediction = self.model.predict(vec[1])
			predictions.append(prediction)
		print "labels", self.results
		print "predictions", predictions
		return predictions

	def regress(self,snapshot):
		regression_results = {'correct':0, 'total': 0, 'unpredicted': 0}
		regression_results['total'] = snapshot.frame_count()
		correct = 0
		total = 0
		
		filenames = snapshot.return_filepaths()
		vectors = self.return_vectors_from_filenames(filenames)
		print "regress :: snapshot size : {0}".format(len(vectors))
		results = snapshot.return_results()
		
		print "regress :: snapshot vehicles : {0}".format(len([res for res in results if res == 1]))
		print "regress :: snapshot bays     : {0}".format(len([res for res in results if res == 0]))

		if self.baygroup:
			for vec in zip(results,vectors,filenames):
				baygrouping = None

				for baypool in self.baygroup.baygroups.keys():
					if self.return_mac_from_filename(vec[2]) in self.baygroup.baygroups[baypool]:
						baygrouping = baypool
						break
				if baygrouping:
					total += 1
					prediction = self.model_dict[baygrouping].predict(vec[1])
					if prediction[0] == vec[0]:
						regression_results['correct'] += 1
				else:
					print "regress :: {0} not found in baygrouping".format(self.return_mac_from_filename(vec[2]))
					regression_results['unpredicted'] += 1
		else:			
			for vec in zip(results,vectors):
				prediction = self.model.predict(vec[1])
				print prediction[0], vec[0]
				if prediction[0] == vec[0]:
					regression_results['correct'] += 1
		print regression_results
		return regression_results

	
	def return_modeldata(self,site,min_date,max_date):
		filepaths,results,vectors = padb.return_modeldata(site,min_date,max_date)
		return [filepaths,results,vectors]

	def create_directory(self,directory):
		if not os.path.exists(directory):
			os.makedirs(directory)

class model_hogsvm(model_rawsvm):

	def return_vectors_from_filenames(self,filenames):
		vectors = []
		#padb.download_images(filenames)
		for filename in filenames:
			image = padb.return_image(filename)
			filepath = os.path.join(padb.image_db_location,filename.replace('/','_'))
			afine_image = paimage.return_affine(filepath)
			#im = skimage.color.rgb2gray(afine_image)
			#im_resize = skimage.transform.resize(afine_image,(100,100))
			vector = hog(afine_image,orientations=9,pixels_per_cell=(25,25),cells_per_block=(3,3))
			#print "hog vector length {0}".format(len(vector))
			vectors.append( vector )
		return vectors 

class model_saltedsvm(model_rawsvm):

	def filter_salt(self,salt,baylist):
		fil = salt[0]
		res = salt[1]
		vec = salt[2]
		filtered_entries =  [(fil,res,vec) for fil,res,vec in zip(fil,res,vec) if self.return_mac_from_filename(fil) in baylist]
		return ([x[0] for x in filtered_entries],[x[1] for x in filtered_entries],[x[2] for x in filtered_entries])

	def build(self):
		if self.baygroup:
			salt_entries = self.return_salt(self.site,self.mindate,self.maxdate)
			self.model_dict = {}
			for baypool in self.baygroup.baygroups.keys():
				print "building :: baypool", baypool
				print "building :: total entries", len(self.entries[0])
				if self.entries:
					entries = self.filter_entries(self.baygroup.baygroups[baypool])
					results = entries[1]
					vectors = entries[2]
					model = svm.SVC()
					clf = clf.fit(vectors,results)	
					self.model_dict[baypool] = model.fit(vectors,results)
					
		else:
			print "not implimented"
		return self.model

	def return_salt(self,site,min_date,max_date):
		filepaths,results,vectors = padb.return_salt(site,min_date,max_date)
		return [filepaths,results,vectors]

class model_ibmsvm(model_rawsvm):

	def __init__(self,site,mindate,maxdate,baygroup):
		self.bay = baygroup
		#super(model_ibm,self).__init__(site,mindate,maxdate)
		model_randomtree.__init__(self,site,mindate,maxdate)

	def create_entries(self):
		random.seed(self.seed)
		entries = padb.return_baygroup_modeldata(self.site,self.mindate,self.maxdate,self.bay)
		print "ibm :: create entries :  {0}".format(len(entries))
		random.shuffle(entries)
		self.entries = entries[0:self.numentries]
		return self.entries

if __name__ == "__main__":
	try: 
		opts,args = getopt.getopt(sys.argv,"hd:d",["dump","build","regress"])
	except getopt.GetoptError:
		sys.exit(2)
	
	'''	
	# create standard model, print results
	test_snapshot_list = [('20141018','1500')]
	results_list = []
	print "init model"
	rawsvm = model_rawsvm(site = 2, maxdate = 20141018, mindate = 20141001)
	print "creating..."
	rawsvm.create_entries()
	print "building..."
	rawsvm.build()
	for snapshot in test_snapshot_list:
		snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
		print "regressing..."
		results_list.append(rawsvm.regress(snap))
	for result in results_list:
		rawsvm.print_regression(result)

	
	# test ibm model
	snap = ss.snapshot(site = 2, datekey = 20141018, timekey = 1500)
	print "init model"
	bay = ['84-6e-b1-00-22-56-1']
	ibm_model = model_ibm(site = 2, maxdate = 20141018, mindate = 20140721,baygroup = bay)
	print "creating..."
	ibm_model.create_entries()
	print "building..."
	ibm_model.build()
	print "regressing..."
	results_ibm_model = ibm_model.regress(snap)
	ibm_model.print_regression(results_ibm_model)
	
	# test salted model
	snap = ss.snapshot(site = 2, datekey = 20141018, timekey = 1500)
	print "init model"
	salted_model = model_saltedtree(site = 2, maxdate = 20141018, mindate = 20140721)
	print "creating..."
	salted_model.create_entries()
	print "building..."
	salted_model.build()
	print "regressing..."
	results_salted_model = salted_model.regress(snap)
	salted_model.print_regression(results_salted_model)
	'''
	# test hog svm model
	test_snapshot_list = [('20141018','1500')]
	results_list = []
	print "init model"
	hogsvm = model_hogsvm(site = 2, maxdate = 20141018, mindate = 20141016)
	print "creating..."
	hogsvm.create_entries()
	print "building..."
	hogsvm.build()
	for snapshot in test_snapshot_list:
		snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
		print "regressing..."
		results_list.append(hogsvm.regress(snap))
	for result in results_list:
		hogsvm.print_regression(result)

