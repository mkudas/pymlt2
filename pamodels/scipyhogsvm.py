import sys,getopt,os
import regress
import random

import pautils.db as padb
import pautils.snapshot as ss
import pautils.baygroup as pabaygroup
import pautils.image as paimage

import numpy as np
import skimage
from skimage.transform import resize
from sklearn import svm



class model_hogsvm():
	def __init__(self,site,mindate,maxdate, baygroup = None):
		self.site = site
		self.entries = None@
		self.numentries = 10000
		self.seed = 10000
		self.model = None
		self.mindate = mindate
		self.maxdate = maxdate
		self.baygroup = baygroup
		self.model_dict = {}

	def return_mac_from_filename(self,filename):
		return filename.split('/')[3].replace('.jpg','')

	def filter_entries(self,baylist):
		fil = self.entries[0]
		res = self.entries[1]
		vec = self.entries[2]
		filtered_entries =  [(fil,res,vec) for fil,res,vec in zip(fil,res,vec) if self.return_mac_from_filename(fil) in baylist]
		return ([x[0] for x in filtered_entries],[x[1] for x in filtered_entries],[x[2] for x in filtered_entries])

	def create_entries(self):
		random.seed(self.seed)
		entries = self.return_modeldata(self.site,self.mindate,self.maxdate)
		entr = [(f,r,v) for f,r,v in zip(entries[0],entries[1],entries[2])]
		random.shuffle(entr)
		self.entries = ([e[0] for e in entr], [e[1] for e in entr], [e[2] for e in entr])
		return self.entries

	def predict(self,vector):
		return self.model.predict(vector)

	def regress_snapshot(model,snapshot):
		results = {'correct':0, 'total': 0}
		results['total'] = len(snapshot[1])
		correct = 0
		vectors = snapshot[2]
		print "snapshot size     : {0}".format(len(vectors))
		print [res for res in results]
		print "snapshot vehicles : {0}".format(len([res for res in results if res == 1]))
		print "snapshot bays     : {0}".format(len([res for res in results if res == 0]))
		predictions = snapshot[1]
		for vec in zip(predictions,vectors):
			res = model.predict(vec[1])
			if res[0] == vec[0]:
				results['correct'] += 1
		return results

	def build_model(model_data):
		print "building model..."
		vectors = model_data[2]
		results = model_data[1]
		print "vectors: {0}".format(len(vectors))
		clf = RandomForestClassifier(n_estimators = 50, max_depth = 50, max_features = 7)
		clf = clf.fit(vectors,results)
		return clf

	def return_modeldata(site,min_date,max_date):
		filepaths,results,vectors = padb.return_modeldata(site,min_date,max_date)
		return (filepaths,results,vectors)

	def return_snapshot(site,date,time):
		filepaths,results,vectors = padb.return_snapshot(site,date,time)
		return (filepaths,results,vectors)

	def format_results(results):
		for key in results.keys():
			print "{0} : {1}".format(key,results[key])

	def create_directory(directory):
		if not os.path.exists(directory):
			os.makedirs(directory)

	def get_filenames(filename):
		with open(filename, 'r') as fh:
			data = fh.readlines()
		line     = [d.strip() for d in data[1:]]
		datalist = [d.split(',') for d in line]
		filenames = [d[1] for d in datalist]
		results  = [int(d[2]) for d in datalist]
		return filenames, results

	def preprocess_images(filenames,inputpath):
		pathname = os.path.join(inputpath,'process_convo/')
		create_directory(pathname)
		for name in filenames:
			try:
				fname = name.replace('/','_')
				out = paimage.return_affine(os.path.join(inputpath,fname))
				fnameout = fname.replace(".jpg","_polycut.jpg")
				skimage.io.imsave(os.path.join(pathname,fnameout))
			except:
				print "file not found",fname
				pass
			
if __name__ == "__main__":
	try: 
		opts,args = getopt.getopt(sys.argv,"hd:d",["dump","build","regress"])
	except getopt.GetoptError:
		sys.exit(2)

	# create standard model, print results
	test_snapshot_list = [('20141018','1500')]
	results_list = []
	print "init model"
	hogvm = model_hogsvm(site = 2, maxdate = 20141018, mindate = 20141001)
	print "creating..."
	hogsvm.create_entries()
	print "building..."
	hogsvm.build()
	for snapshot in test_snapshot_list:
		snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
		print "regressing..."
		results_list.append(rawsvm.regress(snap))
	for result in results_list:
		rawsvm.print_regression(result)
	