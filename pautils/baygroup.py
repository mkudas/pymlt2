import os

class baygroup():
	
	def __init__(self,sitename):
		self.sitename = sitename
		self.baygroup_filelocation = '/home/parkassist/Projects/2014SiteLists/'
		self.baygroups = {}
		self.dirname = os.path.join(self.baygroup_filelocation,self.sitename)
		print "loading baygroups from {0}".format(self.dirname)

		for root,folders,files in os.walk(self.dirname):
			for f in files:
				baygroupname = f.replace('.txt','')
				filename = os.path.join(self.dirname,f)
				with open(filename, 'r') as fh:
					data = fh.readlines()
				data_strip = [f.strip().replace(':','-') for f in data]
				print "loading baygroup",baygroupname
				self.baygroups[baygroupname] = data_strip
		return

	def remove_bays(self,baygroup):
		'''remove_bays will remove all the bays in baygroup from existing baygroups'''

		for group in self.baygroups.keys():
			print group, len(self.baygroups[group])

		for group in self.baygroups.keys():
			print "group", group
			if group == baygroup:
				continue

			for bay in self.baygroups[baygroup]:
				
				if bay in self.baygroups[group]:
					print "removing", bay, "from", group
					self.baygroups[group].remove(bay)

		for group in self.baygroups.keys():
			if group == baygroup:
				continue
			filename = os.path.join(self.dirname,group + '.bak')
			with open(filename, 'w') as fh:
				for bay in self.baygroups[group]:
					fh.write(bay + '\n')



