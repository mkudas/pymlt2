import MySQLdb
import sys,os
sys.path.append('..')

from sklearn.ensemble import RandomForestClassifier
import skimage.io
import threading
import Queue

import pautils.rax as parax

import ConfigParser as CP
config = CP.ConfigParser()
config.read('../config/config.conf')
password = config.get('db','password')
username = config.get('db','user')
host     = config.get('db','host')
image_db_location = config.get('directories','imagelocation')


mltheader = 'ID,FILENAME,BAY_STATE,NUM_VALUES,A1XL,A1Xa,A1Xb,A1YL,A1Ya,A1Yb,A1BL,A1Ba,A1Bb,A1ML,A1Ma,A1Mb,T1XL,T1Xa,T1Xb,T1YL,T1Ya,T1Yb,T1BL,T1Ba,T1Bb,T1ML,T1Ma,T1Mb,A2XL,A2Xa,A2Xb,A2YL,A2Ya,A2Yb,A2BL,A2Ba,A2Bb,A2ML,A2Ma,A2Mb,T2XL,T2Xa,T2Xb,T2YL,T2Ya,T2Yb,T2BL,T2Ba,T2Bb,T2ML,T2Ma,T2Mb,Angle,CentroidX,CentroidY,minA,maxA,minB,maxB,c_maxa,c_mina,c_minb,c_maxb,plane_mse,plane_coeff_x,plane_coeff_y'
feature_string = 'A1XL,A1Xa,A1Xb,A1YL,A1Ya,A1Yb,A1BL,A1Ba,A1Bb,A1ML,A1Ma,A1Mb,T1XL,T1Xa,T1Xb,T1YL,T1Ya,T1Yb,T1BL,T1Ba,T1Bb,T1ML,T1Ma,T1Mb,A2XL,A2Xa,A2Xb,A2YL,A2Ya,A2Yb,A2BL,A2Ba,A2Bb,A2ML,A2Ma,A2Mb,T2XL,T2Xa,T2Xb,T2YL,T2Ya,T2Yb,T2BL,T2Ba,T2Bb,T2ML,T2Ma,T2Mb,Angle,CentroidX,CentroidY,minA,maxA,minB,maxB,c_maxa,c_mina,c_minb,c_maxb,plane_mse,plane_coeff_x,plane_coeff_y'
features = feature_string.split(',')
features = ['feature_vectors.{0}'.format(x) for x in features]
features = ','.join(features)

#image_db_location = '/home/parkassist/Projects/pa_mlt2/imagedb/'
#image_db_location = '/media/parkassist/a8205e17-d679-4acc-84e9-21b22c0b58e9/home/parkassist/Projects/pa_mlt2/imagedb'

class downloadImage(threading.Thread):
	def __init__ (self,queue):
		self.queue = queue
		threading.Thread.__init__(self)

	def run(self):
		filename = self.queue.get()
		filepath = os.path.join(image_db_location,filename.replace('/','_'))
		if os.path.isfile(filepath):
			print "found image {0}".format(filename)
			pass
		else:
			print "not found: retrieving image {0}".format(filename)
			imagefile = parax.return_image(filename)
			filepath = os.path.join(image_db_location,filename.replace('/','_'))
			with open(filepath,'wb') as fh:
				fh.write(bytearray(imagefile))
		self.queue.task_done()
		return

def download_images(filenames):

	if not os.path.exists(image_db_location):
			os.makedirs(image_db_location)

	BATCH_SIZE = 40
	q = Queue.Queue()
	batchnum = len(filenames) / BATCH_SIZE

	for batch in range(batchnum):
		for i in range(BATCH_SIZE):
			t = downloadImage(q)
			t.setDaemon(True)
			t.start()

		for filename in filenames[batch*BATCH_SIZE:batch*BATCH_SIZE + BATCH_SIZE]:
			q.put(filename)
					
		q.join()
	return

def return_mac_from_filename(filename):
	return filename.split('/')[3].replace('.jpg','')

# get images from list of filenames from rackspace	
def return_image(filename):	
	filepath = os.path.join(image_db_location,filename.replace('/','_'))

	if os.path.isfile(filepath):
		#print "found image {0}".format(filename)
		image = skimage.io.imread(filepath)
	else:
		#print "retrieving image {0}".format(filename)
		imagefile = parax.return_image(filename)
		filepath = os.path.join(image_db_location,filename.replace('/','_'))
		#print "filepath {0}".format(filepath)
		with open(filepath,'wb') as fh:
			fh.write(bytearray(imagefile))
		image = skimage.io.imread(filepath)
	return image

def get_images(filenames,folder):
	for filename in filenames:
		image = parax.return_image(filename)
		filepath = os.path.join(folder,filename.replace('/','_'))
		with open(filepath,'wb') as fh:
			fh.write(bytearray(image))
	return

# output mlts 
def format(prediction, vector, num, name):
	vecstring = ','.join([str(vec) for vec in vector])
	return '{0},{1},{2},62,{3}\n'.format(num,name,prediction,vecstring)

def format_mlt(results,vectors,filenames,outputfilename):
	with open(outputfilename,'w') as fh:
		fh.write(mltheader + '\n')
		for i,row in enumerate(zip(results,vectors,filenames)):
			row_string = format(row[0],row[1],i,row[2])
			fh.write(row_string)

# write a function that returns a snapshot
def return_snapshot(site,date,time):
	command = 'SELECT detection_frames.filename, detection_frames.predicted_occupied, {1} FROM detection_frames JOIN feature_vectors ON detection_frames.id = feature_vectors.detection_frame_id WHERE site_id = {0} and detection_frames.date_key = {2} and detection_frames.time_key = {3}'.format(site,features,date,time)
	query_result = connect(command)
	filepaths,results,vectors = format_results(query_result)
	return filepaths,results,vectors

def return_modeldata(site,min_date,max_date):
	command = 'SELECT detection_frames.filename, detection_frames.predicted_occupied, {1} FROM detection_frames JOIN feature_vectors ON detection_frames.id = feature_vectors.detection_frame_id WHERE site_id = {0} and detection_frames.date_key > {2} and detection_frames.date_key < {3}'.format(site,features,min_date,max_date)
	query_result = connect(command)
	filepaths,results,vectors = format_results(query_result)
	return filepaths,results,vectors

def filter_baygroup(filepaths,results,vectors,baygroup):
	filtered_results = [(fil,res,vec) for fil,res,vec in zip(filepaths,results,vectors) if return_mac_from_filename(fil) in baygroup]
	return filtered_results[0],filtered_results[1],filtered_results[2]

def return_baygroup_modeldata(site,min_date,max_date,baygroup):
	command = 'SELECT detection_frames.filename, detection_frames.predicted_occupied, {1} FROM detection_frames JOIN feature_vectors ON detection_frames.id = feature_vectors.detection_frame_id WHERE site_id = {0} and detection_frames.date_key > {2} and detection_frames.date_key < {3}'.format(site,features,min_date,max_date)
	query_result = connect(command)
	filepaths,results,vectors = format_results(query_result)
	filteredfilepaths,filteredresults,filteredvectors = filter_baygroup(filepaths,results,vectors,baygroup)
	print "in filter {0}".format(len(filteredfilepaths))
	return zip(filteredfilepaths,filteredresults,filteredvectors)

def return_bay_predictions(site,min_date,max_date,bays_ids):
	print bays_ids, type(bays_ids)
	command = 'SELECT base_model_prediction FROM detection_frames WHERE site_id = {0} and detection_frames.date_key > {2} and detection_frames.date_key < {3} and bay_id = {4}'.format(site,features,min_date,max_date,bays_ids)
	query_result = connect(command)
	
	return query_result

def return_salt(site,min_date,max_date):
	command = 'SELECT detection_frames.filename, detection_frames.predicted_occupied, {1} FROM detection_frames JOIN feature_vectors ON detection_frames.id = feature_vectors.detection_frame_id WHERE site_id = {0} and detection_frames.date_key > {2} and detection_frames.date_key < {3} and detection_frames.human_response != detection_frames.predicted_occupied'.format(site,features,min_date,max_date)
	query_result = connect(command)
	filepaths,results,vectors = format_results(query_result)
	return (filepaths,results,vectors)

# write a function that gets featuredata from the local mysql database
def connect(command):
	results = []
	global password ; global host ; global username;
	if 'localhost' in host:		
		db = MySQLdb.connect(host=host,user=username,passwd=password,db='services',unix_socket = '/var/run/mysqld/mysqld.sock')
	else:
		db = MySQLdb.connect(host=host,user=username,passwd=password,db='services')
	cur = db.cursor()
	cur.execute(command)
	for row in cur.fetchall():
		results.append(row)
	return results

def check_vector(vector):
	return all([type(x) is float for x in vector])

def format_results(query_result):
	results = []
	vectors = []
	filepaths = []
	print "format_results: {0}".format(len(query_result))
	for row in query_result:
		if check_vector(row[2:]):
			filepaths.append(row[0])
			results.append(int(row[1]))
			#print [x for x in row[1:]]
			vectors.append([x for x in row[2:]])
	return filepaths,results,vectors	

def return_vectors(site,baygroup):
	site_dict = {'stratford-city': 2}
	command = 'SELECT detection_frames.filename, detection_frames.predicted_occupied, {1} FROM detection_frames, feature_vectors WHERE detection_frames.id = feature_vectors.id and site_id = {0}'.format(site,features)
	query_result = connect(command)
	filepaths,results,vectors = format_results(query_result)
	return filepaths,results,vectors

def return_model(results,vectors):
	clf = RandomForestClassifier(n_estimators = 10)
	clf = clf.fit(vectors,results)
	return clf

def create_model(site,baygroup):
	filepaths,results,vectors = return_vectors(site,baygroup)
	model = return_model(results[0:100],vectors[0:100])
	return model

# WRITE FUNCTION THAT RETURNS THE IMAGE FROM THE DATABASE

def return_rax_image(site,filename):
	image = 0
	'''
	if check_local_filestore(site,filename):
		with open()
			image = fh.read()
	else:
		cont = parax.return_rackspace_container(site,filename)
		image = parax.return_rackspace_image(cont,filename)
	'''
	return image

# WRITE FUNCTION THAT RETURNS 120x120 transformed patch from ycc


# write a function that breaks down the image into transfrormed polygon matrix, HOG vector, RT vector, modified RT vector and 
#command = 'SELECT * FROM detection_frames WHERE detection_frames.site_id = 2 INNER JOIN feature_vectors ON feature_vectors.id = detection_frames.feature_vector_id  LIMIT 10'


