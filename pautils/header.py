import json
#from PIL import Image
import json

def add_header(filename,status):

    if not("empty" == status or "car" == status):
        print "wrong status"
        return

    with open(filename,"rb") as file_in:
        my_file = file_in.read()

    # Extract comment meta data from the JPG header. 
    # Search for 0xFFFE comment marker followed by 2 bytes length.
    # The length is a total sum of comment string and length field.
    # Convert string to bytearray
    img_ba = bytearray(my_file)
    new_ba = bytearray()
    head_ba = None
    head_str = ""
    comment_len = 0
    len_idx = 0;
    idx = 0

    # Find comment tocken and extract length. Scan the first 100 bytes, no reason to go further.
    # JPEG/JFIF spec should have comment after 0x16 + 2 header offset
    for idx in xrange(200):
        if img_ba[idx] == 0xFF:
            if img_ba[idx+1] == 0xFE:
                idx = idx+2
                len_idx = idx
                # Extract length of comment
                comment_len = (((img_ba[idx] & 0xFF) << 8) | (img_ba[idx+1] & 0xFF))-2
                head_ba = bytearray(comment_len)
                # Copy byte array for JSON comment only
                # from index + size_field_len to index + comment_size + size_field_len
                head_ba[:] = img_ba[idx+2:idx+2+comment_len]
                head_str = str(head_ba.decode("utf-8"))
                break
    # Convert to JSON
    #try:
    #print head_str
    img_jh = json.loads(head_str)
    if "human_response" in img_jh.keys():
        print filename, "already has key", img_jh['human_response'], " tried ", status
        return
    #except Exception as jlex:
        print "Unable to load JSON\n"              
    # Add local info to JSON comments
    try:
        img_jh['human_response'] = status
    except Exception as mcex:
        print "Unable to get additional headers\n"

    # Assemble bytearray with new comment header. idx - points to length field
    new_ba = img_ba[0:len_idx+2]
    new_head_str = json.dumps(img_jh)
    new_ba += bytearray(new_head_str)
    new_ba += img_ba[len_idx+2+comment_len:]
    new_len = len(new_head_str)+2
    new_ba[len_idx] = ((new_len >> 8) & 0xFF)
    new_ba[len_idx+1] = (new_len & 0xFF)

    dataout = str(new_ba)
    with open(filename,'wb') as fh:
        fh.write(dataout)


def return_json_header(json_string):
    if json_string:
        a = json.loads(json_string)
    else:
        a = {}
    return a

def split_jpg(jpg):
    try:
        start_index = jpg.find("{")
        #stupid hack for files with "{{""
        if jpg[start_index+1] == "{":
           start_index += 1 
        header_text = jpg[start_index:].split("\x00")
        start_index = header_text[0].find("{")
        end_index = header_text[0].rfind("}")
        json_string = header_text[0][start_index:end_index+1]
    except:
        json_string = []
    return json_string

def split_ycc(ycc):
    try:
        start_index = ycc.find("{")
        end_index = ycc.find("}\x00")
        json_string = ycc[start_index:end_index+1]
    except:
        json_string = []
    return json_string

def read_whole_file(path):
    with open(path,"rb") as file_in:
        my_file = file_in.read()
    return my_file

def return_header_from_jpgstring(jpg_string):
    header = split_jpg(jpg_string)
    json_header = return_json_header(header)
    return json_header

def return_header_from_yccstring(jpg_string):
    header = split_ycc(jpg_string)
    json_header = return_json_header(header)
    return json_header

def determine_format(imgstrg):
    imageformat = 'jpg'
    if '// PA-YCC' in imgstrg:
        imageformat = 'ycc'
    return imageformat

def return_header(filename):
    imgstrg = read_whole_file(filename)
    imageformat = determine_format(imgstrg)
    if imageformat == 'ycc':
        json_header = return_header_from_yccstring(imgstrg)
    else:
        json_header = return_header_from_jpgstring(imgstrg)
    return json_header
