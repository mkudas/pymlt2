import skimage
import header
from skimage.transform import warp, PiecewiseAffineTransform
from skimage import io
import numpy as np
import cv2
import math

def return_rt_string(image):
	image_header = header.return_header(image)
	values = image_header['values']
	return list(values)

def return_polygon(image):
	image_header = header.return_header(image)
	polygon = image_header['polygon']
	return polygon

def transfrorm(image,poly):
	with open(image,'r') as fh:
		image = rh.read()
	return out_image

def read_image_transform(image):
	poly = return_polygon(image)
	out_image = transform(image,poly)
	return

def return_roi(imagename,poly):
	return im

def return_poly_roi(imagename):
	# polygon
	poly = return_polygon(imagename)
	# roi cut
	im = return_roi(imagename,poly)
	return im

def return_affine(imagefilename, gray = True):
	poly = return_polygon(imagefilename)
	im = skimage.io.imread(imagefilename,as_grey=gray)
	if gray:
		(y,x) = im.shape
		dst = [[int(poly['x0'] * x), int(poly['y0'] * y)],[int(poly['x1'] * x), int(poly['y1'] * y)],[int(poly['x2'] * x),int( poly['y2'] * y)],[int(poly['x3'] * x), int(poly['y3'] * y)]]
		src = [[0,0],[100, 0],[100,100],[0,100]]
		
		tform = PiecewiseAffineTransform()
		tform.estimate(np.array(src),np.array(dst))
		out = warp(im,tform)
	else:
		(y,x,c) = im.shape
		dst = [[int(poly['x0'] * x), int(poly['y0'] * y)],[int(poly['x1'] * x), int(poly['y1'] * y)],[int(poly['x2'] * x),int( poly['y2'] * y)],[int(poly['x3'] * x), int(poly['y3'] * y)]]
		src = [[0,0],[100, 0],[100,100],[0,100]]
		
		tform = PiecewiseAffineTransform()
		tform.estimate(np.array(src),np.array(dst))
		out = warp(im,tform)

	return out[0:100,0:100]

def return_warp(imagefilename, gray = True):
	poly = return_polygon(imagefilename)
		
	#im = skimage.io.imread(imagefilename,as_grey=True)

	if gray:
		im = cv2.imread(imagefilename,0)
		(y,x) = im.shape
		src = [[int(poly['x0'] * x), int(poly['y0'] * y)],[int(poly['x1'] * x), int(poly['y1'] * y)],[int(poly['x2'] * x),int( poly['y2'] * y)],[int(poly['x3'] * x), int(poly['y3'] * y)]]
		src = np.array(src,np.float32)
		dst = [[0,0],[100, 0],[100,100],[0,100]]
		dst = np.array(dst,np.float32)

		transformMat = cv2.getPerspectiveTransform(src,dst)
		out = cv2.warpPerspective(im,transformMat,(100,100))
	else:
		im = cv2.imread(imagefilename,1)
		out = np.empty([100,100,3])
		(y,x,c) = im.shape
		src = [[int(poly['x0'] * x), int(poly['y0'] * y)],[int(poly['x1'] * x), int(poly['y1'] * y)],[int(poly['x2'] * x),int( poly['y2'] * y)],[int(poly['x3'] * x), int(poly['y3'] * y)]]
		src = np.array(src,np.float32)
		dst = [[0,0],[100, 0],[100,100],[0,100]]
		dst = np.array(dst,np.float32)
		transformMat = cv2.getPerspectiveTransform(src, dst)
		out[:,:,0] = cv2.warpPerspective(im[:,:,0],transformMat,(100,100))
		out[:,:,1] = cv2.warpPerspective(im[:,:,1],transformMat,(100,100))
		out[:,:,2] = cv2.warpPerspective(im[:,:,2],transformMat,(100,100))	
	return out

def dogradientangleentropy(image):
	vsobel1 = skimage.filter.vsobel(image[:,:,1])
	hsobel1 = skimage.filter.hsobel(image[:,:,1])
	radtodeg = 180.0 / 3.1415926
	hist = np.histogram([np.arctan(x,y) * radtodeg for x,y in zip(hsobel1.flatten(),vsobel1.flatten())],bins=360)
	total = sum(hist)
	norm_bins = [x/total for x in hist]
	entropy = sum([- x * log2(x) for x in norm_bins])
	return entropy

def getinvmatrix():
	'''
	sumX = 0 ; sumY = 0 ; sumXX = 0 ; sumYX = 0 ; sumYY = 0;
	for x in range(100):
		for y in range(100):
			sumX += x
			sumY += y 
			sumXX += x*x
			sumYX += y*x
			sumYY += y*y

	numpoints = float(100*100)
	mat = np.zeros(9)
	print mat
	mat[0] = sumXX / numpoints
	mat[1] = sumYX / numpoints
	mat[3] = sumYX / numpoints
	mat[2] = sumX / numpoints
	mat[6] = sumX / numpoints
	mat[5] = sumY / numpoints
	mat[7] = sumY / numpoints
	mat[8] = 1.0
	mat.resize([3,3])
	inv = np.linalg.inv(mat)
	'''
	inv = np.array([[  3.28350000e+03,   2.45025000e+03,   4.95000000e+01],
       [  2.45025000e+03,   0.00000000e+00,   4.95000000e+01],
       [  4.95000000e+01,   4.95000000e+01,   1.00000000e+00]],dtype=float)
	
	return inv

def getvaluevector(labimage):
	sXZ = 0.0 ; sYZ = 0.0 ; sZ = 0.0 ; 
	x,y,c = labimage.shape
	num = float(x * y)
	for i in range(x):
		for j in range(y):
			sXZ += labimage[i,j,0] * x
			sYZ += labimage[i,j,0] * y
			sZ  += labimage[i,j,0]
	return sXZ / num , sYZ / num, sZ / num

def doplanemse(labimage, planeCoeffs):
	x,y,c = labimage.shape
	numpoints = float(x * y)
	mse = 0
	planecoeffs = np.zeros(3)
	for i in range(x):
		for j in range(y):
			lval = labimage[i,j,0]
			predVal = i * planecoeffs[0] + j * planecoeffs[1] + planecoeffs[2]
			diff = lval - predVal
			mse += diff * diff
	return mse / numpoints

def doplanecoeffs(labimage):
	matInv = getinvmatrix()
	vals = getvaluevector(labimage)
	planecoeffs =  np.dot(matInv ,vals)
	return [planecoeffs[0],planecoeffs[1], doplanemse(labimage,planecoeffs)]

def docolorfeature(image):
	NUM_BINS = 256
	#lab = cv2.cvtColor(image.astype(np.uint8),cv2.COLOR_RGB2LAB)
	hist_2d = np.zeros(NUM_BINS * NUM_BINS,dtype=int)

	a = image[:,:,1].flatten()
	b = image[:,:,2].flatten()

	for x in zip(a,b):
		hist_2d[x[1] * NUM_BINS + x[0]] = hist_2d[x[1] * NUM_BINS + x[0]] + 1

	minA = 255; minB = 255; maxA = 0; maxB = 0;
	centroidX_unweighted = 0.0; centroidY_unweighted = 0.0;
	num_non_zero_bins = 0;

	for a in range(NUM_BINS):
		for b in range(NUM_BINS):
			bin_val = hist_2d[b*NUM_BINS + a]
			
			if bin_val > 0:
				centroidX_unweighted += a
				centroidY_unweighted += b
				num_non_zero_bins += 1
				if a > maxA:
					maxA = a
				if a < minA:
					minA = a
				if b > maxB:
					maxB = b
				if b < minB:
					minB = b
	if num_non_zero_bins > 0:
		centroidY_unweighted /= num_non_zero_bins
		centroidX_unweighted /= num_non_zero_bins

	return [centroidX_unweighted,centroidY_unweighted,minA,maxA,minB,maxB]

def domagnitude(image):
	vsobel1 = skimage.filter.vsobel(image)
	hsobel1 = skimage.filter.hsobel(image)
	s1 = [math.sqrt(x**2 + y **2) for x,y in zip(vsobel1.flatten(),hsobel1.flatten())]

	vsobel1 = skimage.filter.vsobel(vsobel1)
	hsobel1 = skimage.filter.hsobel(hsobel1)
	s2 = [math.sqrt(x**2 + y **2) for x,y in zip(vsobel1.flatten(),hsobel1.flatten())]
	return [sum(s1),sum(s2)]

def doabs(image):
	sobel   = skimage.filter.sobel(image)
	sobel2  = skimage.filter.sobel(sobel)
	h,w  = image.shape[:2]
	N = float(h * w)
	return [sum(sobel.flatten()) / N, sum(sobel2.flatten()) / N]

def dox(image):
	vsobel1 = skimage.filter.vsobel(image)
	vsobel2 = skimage.filter.vsobel(vsobel1)
	h,w  = image.shape[:2]
	N = float(h * w)
	return [sum(vsobel2.flatten()) / N, sum(vsobel1.flatten()) / N]

def doy(image):
	hsobel1 = skimage.filter.hsobel(image)
	hsobel2 = skimage.filter.hsobel(hsobel1)
	h,w  = image.shape[:2]
	N = float(h * w)
	return [sum(hsobel1.flatten()) / N, sum(hsobel2.flatten()) / N]

def return_42(image):
	vector = []
	funcs = [domagnitude,doabs,dox,doy]
	for f in funcs:
		vector = vector + f(image)
	return vector

def return_cv_vectors(imagefilename):
	vector = []
	im = return_warp(imagefilename,gray=False)
	lab = cv2.cvtColor(im.astype(np.uint8),cv2.COLOR_RGB2LAB)

	#for each scale
	multiscale_list = [lab[:,:,:],lab[0:50,0:50,:],lab[50:,0:50,:],lab[50:,0:50,:],lab[50:,0:50,:]]
	#multiscale_list = [lab[:,:,:]]
	for image in multiscale_list:
		for i in range(3):
			vector = vector + return_42(image[:,:,i])			
		
		vector = vector + docolorfeature(image) +  doplanecoeffs(lab)
	return vector

#import matplotlib.pyplot as plt
#plt.imshow(sobel,cmap='gray') 
#plt.show()
#x,y,c = image.size
