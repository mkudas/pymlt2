import sys,os
sys.path.append('..')

import pyrax


pyrax.set_setting("identity_type", "rackspace")
pyrax.set_default_region('DFW')
pyrax.set_credentials("mark.kudas","bd2a5613bd654feabc8d514ae3d88614")
cf = pyrax.cloudfiles

def parse_name(filename):
	print filename
	site = filename.split('/')[0]
	date = '-'.join(filename.split('/')[1].split('-')[0:2])
	return '-'.join([site,date]),filename

def return_rackspace_container(contname):
	containers =  cf.list_containers()
	
	if contname in containers:
		cont = cf.get_container(contname)
		return cont
	else:
		return None
	
def return_rackspace_image(cont,filename):
	objs = cont.get_objects(prefix=filename)
	image = None
	for obj in objs:
		if obj.name == filename:
			image = obj.get()
			break
	return image

def return_image(filename):
	image = None
	contname,objname = parse_name(filename)
	cont   = return_rackspace_container(contname)
	if cont:
		image  = return_rackspace_image(cont,objname)
	return image

if __name__ == "__main__":

	site = 'calgary'
	filename = []
	site = []

	cont = return_rackspace_container(site,filename)
	image = return_rackspace_image(cont,filename)
	
	with open('tmp.ycc','wb') as fh:
		fh.write(bytearray(image))
	
