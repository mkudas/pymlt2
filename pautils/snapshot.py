import pautils.db as db

class snapshot():
	def __init__(self,site,datekey,timekey,test=False):
		self.site = site
		self.datekey = datekey
		self.timekey = timekey
		self.filepaths = None
		self.vectors = None
		self.results = None
		self.test = test

	def return_vectors(self):
		if not self.vectors:
			self.filepaths,self.results,self.vectors = padb.return_snapshot(self.site,self.datekey,self.timekey)
		if self.test:
			return self.vectors[0:10]
		return self.vectors

	def return_results(self):
		if not self.results:
			self.filepaths,self.results,self.vectors = padb.return_snapshot(self.site,self.datekey,self.timekey)
		if self.test:
			return self.results[0:10]
		return self.results

	def return_filepaths(self):
		if not self.filepaths:
			self.filepaths,self.results,self.vectors = padb.return_snapshot(self.site,self.datekey,self.timekey)

		if self.test:
			return self.filepaths[0:10]
		return self.filepaths

	def frame_count(self):
		if not self.results:
			self.filepaths,self.results,self.vectors = padb.return_snapshot(self.site,self.datekey,self.timekey)
		if self.test:
			return len(self.filepaths[0:10])
		return len(self.results)



