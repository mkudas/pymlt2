import sys,getopt,os
import padb
import skimage
import paimage

convopath = "home/parkassist/Projects/2014DeepLearning/neural-networks-and-deep-learning/code"
if convopath not in sys.path:
	sys.path.append(convopath)


def regress_snapshot(model,snapshot):
	results = {'correct':0, 'total': 0}
	results['total'] = len(snapshot[1])
	correct = 0
	vectors = snapshot[2]
	print "snapshot size     : {0}".format(len(vectors))
	print [res for res in results]
	print "snapshot vehicles : {0}".format(len([res for res in results if res == 1]))
	print "snapshot bays     : {0}".format(len([res for res in results if res == 0]))
	predictions = snapshot[1]
	for vec in zip(predictions,vectors):
		res = model.predict(vec[1])
		if res[0] == vec[0]:
			results['correct'] += 1
	return results

def build_model(model_data):
	print "building model..."
	vectors = model_data[2]
	results = model_data[1]
	print "vectors: {0}".format(len(vectors))
	clf = RandomForestClassifier(n_estimators = 50, max_depth = 50, max_features = 7)
	clf = clf.fit(vectors,results)
	return clf

def return_modeldata(site,min_date,max_date):
	filepaths,results,vectors = padb.return_modeldata(site,min_date,max_date)
	return (filepaths,results,vectors)

def return_snapshot(site,date,time):
	filepaths,results,vectors = padb.return_snapshot(site,date,time)
	return (filepaths,results,vectors)

def format_results(results):
	for key in results.keys():
		print "{0} : {1}".format(key,results[key])

def create_directory(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)

def get_filenames(filename):
	with open(filename, 'r') as fh:
		data = fh.readlines()
	line     = [d.strip() for d in data[1:]]
	datalist = [d.split(',') for d in line]
	filenames = [d[1] for d in datalist]
	results  = [int(d[2]) for d in datalist]
	return filenames, results

def preprocess_images(filenames,inputpath):
	pathname = os.path.join(inputpath,'process_convo/')
	create_directory(pathname)
	for name in filenames:
		try:
			fname = name.replace('/','_')
			out = paimage.return_affine(os.path.join(inputpath,fname))
			fnameout = fname.replace(".jpg","_polycut.jpg")
			skimage.io.imsave(os.path.join(pathname,fnameout))
		except:
			print "file not found",fname
			pass
		
if __name__ == "__main__":
	try: 
		opts,args = getopt.getopt(sys.argv,"hd:d",["dump","build","regress"])
	except getopt.GetoptError:
		sys.exit(2)
	
	if 'process' in args:
		filenames,results = get_filenames(mltname)
		preprocess_images(filenames)

	elif 'buildmodel' in args:
		pass

		

