import sys,getopt,os
import regress
import padb
from sklearn.ensemble import RandomForestClassifier

def regress_snapshot(model,snapshot):
	results = {'correct':0, 'total': 0}
	results['total'] = len(snapshot[1])
	correct = 0
	vectors = snapshot[2]
	print "snapshot size     : {0}".format(len(vectors))
	print [res for res in results]
	print "snapshot vehicles : {0}".format(len([res for res in results if res == 1]))
	print "snapshot bays     : {0}".format(len([res for res in results if res == 0]))
	predictions = snapshot[1]
	for vec in zip(predictions,vectors):
		res = model.predict(vec[1])
		if res[0] == vec[0]:
			results['correct'] += 1
	return results

def change_from_default(max_depth = 50, max_features = 7, n_estimators = 50):
	self.max_depth = max_depth;
	self.max_features = max_features;
	self.n_estimators = n_estimators;


def build_model(model_data):
	print "building model..."
	vectors = model_data[2]
	results = model_data[1]
	print "vectors: {0}".format(len(vectors))
	clf = RandomForestClassifier(n_estimators = self.n_estimators, max_depth = self.max_depth, max_features = self.max_features)
	clf = clf.fit(vectors,results)
	return clf

def return_modeldata(site,min_date,max_date):
	filepaths,results,vectors = padb.return_modeldata(site,min_date,max_date)
	return (filepaths,results,vectors)

def return_snapshot(site,date,time):
	filepaths,results,vectors = padb.return_snapshot(site,date,time)
	return (filepaths,results,vectors)

def format_results(results):
	for key in results.keys():
		print "{0} : {1}".format(key,results[key])

def create_directory(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)

if __name__ == "__main__":
	try: 
		opts,args = getopt.getopt(sys.argv,"hd:d",["dump","build","regress"])
	except getopt.GetoptError:
		sys.exit(2)
	
	if 'dump' in args:
		site = 2 ; max_date = 20141018 ; min_date = 20141008;
		date_key = '20141106-112300'
		store_directory = './images/{0}/{1}/'.format('calgary',date_key)
		create_directory(store_directory)
		filenames,results,vectors = return_modeldata(site,min_date,max_date)
		print type(vectors),type(results)
		print vectors[0]
		#padb.get_images(filenames,store_directory)
		filename = 'calgary' + date_key + '.mlt'
		pathname = os.path.join(store_directory,filename)
		padb.format_mlt(results,vectors,filenames,pathname)

	elif 'save' in args:
		# create model
		site = 2 ; max_date = 20141018 ; min_date = 20140721;
		#model_data = return_modeldata(site,min_date,max_date)
		#model      = build_model(model_data)

		# regress model
		site = 2 ; date = 20141018 ; time = 1500 ;
		#snapshot = return_snapshot(site,date,time)
		#results  = regress_snapshot(model,snapshot)

		#print results
		#format_results(results)

