import model_convonet as convo

'''
# build model without baygroups
model = convo.model_convonet(site = 2, maxdate = 20141018, mindate = 20141016, n_samples = 1100)
model.create_entries()
model.preprocess()
model.create_mnist_package()
del model
'''


snapshot = ('20141018','1500')
model = convo.model_convonet(site = 2, maxdate = 20141018, mindate = 20141016, n_samples = 11000)
model.return_convoed_snapshot(2, snapshot[0], snapshot[1])
del model 


'''
import cPickle
import gzip

test = '/home/parkassist/Projects/pa_mlt2/data/mnist.pkl.gz'
dataname = "pa_cnn_training.pkl.gz"
validationname = "pa_cnn_validation.pkl.gz"

f = gzip.open(dataname, 'rb')
train_set,test_set = cPickle.load(f)
f.close()

f = gzip.open(validationname, 'rb')
(valid_vectors,valid_results) = cPickle.load(f)
f.close()

f = gzip.open(test, 'rb')
mn_train_set,mn_test_set,mn_valid_set = cPickle.load(f)
f.close()


print len(train_set[0]), type(train_set[0]), type(train_set[1])
print train_set[0][0].size, type(train_set[1][1])
print len(test_set[0]), type(test_set[0]), type(test_set[1])
print test_set[0][0].size, type(test_set[1][1])
print len(valid_vectors), type(valid_vectors), type(valid_results)
print valid_vectors[0][0].size, type(valid_vectors[1][1])

print len(mn_train_set[0]), type(mn_train_set[0]), type(mn_train_set[1])
print mn_train_set[0][0].size, type(mn_train_set[1][1])
print len(mn_test_set[0]), type(mn_test_set[0]), type(mn_test_set[1])
print mn_test_set[0][0].size, type(mn_test_set[1][1])
print len(mn_valid_set[0]), type(mn_valid_set[0]), type(mn_valid_set[1])
print mn_valid_set[0][0].size, type(mn_valid_set[1][1])
filename = 'pa_cnn.pkl'
#cPickle.dump((pa_trainset_vectors,pa_trainset_results),(pa_testset_vectors,pa_testset_results),(pa_validset_vectors,pa_validset_results)),open(filename,'w'))

#print len(pa_trainset_vectors), type(pa_trainset_vectors), type(pa_trainset_results)
#print len(pa_testset_vectors), type(pa_testset_vectors), type(pa_testset_results)
#print len(pa_validset_vectors), type(pa_validset_vectors), type(pa_validset_results)
'''
