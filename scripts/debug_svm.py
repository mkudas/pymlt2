import os
import baygroup
import model_rawsvm as rawsvm
import snapshot as ss
import cPickle

results_list = []
#bays = baygroup.baygroup('Stratford_missing30')
#snapshot_list = [('20141018','1500'),('20141018','1600'),('20141018','0900'),('20141017','1600'),('20141017','1400'),('20141017','0700')]
snapshot_list = [('20141018','1500')]

hogsvmfilename = './vectordb/rawsvm.pkl'

hogsvm_model = rawsvm.model_rawsvm(site = 2, maxdate = 20141018, mindate = 20140701, n_samples = 200)
#hogsvm_model = rawsvm.model_hogsvm(site = 2, maxdate = 20141018, mindate = 20141010)

# build model without baygroups
if not (os.path.isfile(hogsvmfilename)):
	print "generating"
	results = hogsvm_model.create_entries()
	cPickle.dump(results,open(hogsvmfilename,'w'))
else:
	print "loading data"
	results = cPickle.load(open(hogsvmfilename,'r'))

newvectors = results[1][0:3]
#newresults = results[0][0:4]
newresults = [0,0,1]

hogsvm_model.load_results((newresults,newvectors))
hogsvm_model.build()
predictions = hogsvm_model.test_regress()


'''
for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	results_list.append(hogsvm_model.regress(snap))

totalbays = sum([d['total'] for d in results_list])
correctbay = sum([d['correct'] for d in results_list])
print correctbay/float(totalbays)
'''

