import sys
sys.path.append('../pautils/')
sys.path.append('..')
print sys.path

import pamodels.randomtree as mrt

# this is an example of how to create a model and regress a snapshot using the machine learning tool

# optional, first create a baygroup
# this is the name of a directory in ./baygroups/<baygroupname>
# in this case <baygroupname> is Stratford_missing
# for each text file in the directory, a baygroup will be made of the macs


# now create a model
# this will create a model from the dabatase, using site = 2 (stratford), and using data between mindate and max data
# you can also set the number of feature vectors to use, default = 10000
# > rawsvm_model = rawsvm.model_rawsvm(site = 2, maxdate = 20141018, mindate = 20140701, samples = 5000)
# you can also use the baygroup to create multiple models for a site
# > rawsvm_model = rawsvm.model_rawsvm(site = 2, maxdate = 20141018, mindate = 20140701, baygroup = bays)

model = mrt.model_randomtree(site = 131, mindate = 20150501, maxdate = 20150519)

# create entries will pull the data from the local database
model.download_images()
