import MySQLdb
import sys
from sklearn.ensemble import RandomForestClassifier
import parax

feature_string = 'A1XL,A1Xa,A1Xb,A1YL,A1Ya,A1Yb,A1BL,A1Ba,A1Bb,A1ML,A1Ma,A1Mb,T1XL,T1Xa,T1Xb,T1YL,T1Ya,T1Yb,T1BL,T1Ba,T1Bb,T1ML,T1Ma,T1Mb,A2XL,A2Xa,A2Xb,A2YL,A2Ya,A2Yb,A2BL,A2Ba,A2Bb,A2ML,A2Ma,A2Mb,T2XL,T2Xa,T2Xb,T2YL,T2Ya,T2Yb,T2BL,T2Ba,T2Bb,T2ML,T2Ma,T2Mb,Angle,CentroidX,CentroidY,minA,maxA,minB,maxB,c_maxa,c_mina,c_minb,c_maxb,plane_mse,plane_coeff_x,plane_coeff_y'
features = feature_string.split(',')
features = ['feature_vectors.{0}'.format(x) for x in features]
features = ','.join(features)

# write a function that gets featuredata from the local mysql database
def connect(command):
	results = []
	db = MySQLdb.connect(host='localhost',user='root',passwd='1qazxsw2',db='services',unix_socket = '/var/run/mysqld/mysqld.sock')
	cur = db.cursor()
	cur.execute(command)
	for row in cur.fetchall():
		results.append(row)
	return results

def check_vector(vector):
	return all([type(x) is float for x in vector])

def format_results(query_result):
	results = []
	vectors = []
	filepaths = []
	#print len(query_result)
	for row in query_result:
		if check_vector(row[1:]):
			filepaths.append(row[0])
			results.append(int(row[1]))
			#print [x for x in row[1:]]
			vectors.append([x for x in row[2:]])
	return filepaths,results,vectors	

def return_vectors(site,baygroup):
	site_dict = {'stratford-city': 2}
	command = 'SELECT detection_frames.filename, detection_frames.predicted_occupied, {1} FROM detection_frames, feature_vectors WHERE detection_frames.id = feature_vectors.id and site_id = {0}'.format(2,features)
	query_result = connect(command)
	filepaths,results,vectors = format_results(query_result)
	return filepaths,results,vectors

def return_model(results,vectors):
	clf = RandomForestClassifier(n_estimators = 10)
	clf = clf.fit(vectors,results)
	return clf

def create_model(site,baygroup):
	filepaths,results,vectors = return_vectors(site,baygroup)
	model = return_model(results[0:100],vectors[0:100])
	return model

# WRITE FUNCTION THAT RETURNS THE IMAGE FROM THE DATABASE

def return_rax_image(site,filename):
	image = 0
	'''
	if check_local_filestore(site,filename):
		with open()
			image = fh.read()
	else:
		cont = parax.return_rackspace_container(site,filename)
		image = parax.return_rackspace_image(cont,filename)
	'''
	return image

# WRITE FUNCTION THAT RETURNS 120x120 transformed patch from ycc


# write a function that breaks down the image into transfrormed polygon matrix, HOG vector, RT vector, modified RT vector and 
#command = 'SELECT * FROM detection_frames WHERE detection_frames.site_id = 2 INNER JOIN feature_vectors ON feature_vectors.id = detection_frames.feature_vector_id  LIMIT 10'


