
import sys,os,shutil
sys.path.append('../pautils')

import header

import ConfigParser as CP
config = CP.ConfigParser()
config.read('../config/config.conf')
password = config.get('db','password')
username = config.get('db','user')
host     = config.get('db','host')
image_db_location = config.get('directories','imagelocation')

baypath = os.path.join(image_db_location,'bays/')
vehpath = os.path.join(image_db_location,'vehs/')
if not os.path.exists(baypath):
	print baypath
	os.makedirs(baypath)
if not os.path.exists(vehpath):
	os.makedirs(vehpath)

for root,dirs,files in os.walk(image_db_location):
	for f in files:
		filename =  os.path.join(root,f)
		fileheader = header.return_header(filename)
		
		if fileheader['prediction'] > 0.5:
			destination = vehpath
			print destination
			print filename
			shutil.move(filename,destination)
		else:
			destination = baypath
			print destination
			print filename
			shutil.move(filename,destination)






