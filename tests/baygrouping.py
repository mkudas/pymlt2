import baygroup
import model_randomtree as mrt
import snapshot as ss
	
results_list = []
bays = baygroup.baygroup('Stratford_missing30')

snapshot_list = [('20141018','1500'),('20141018','1600'),('20141018','0900'),('20141017','1600'),('20141017','1400'),('20141017','0700')]
# build model with baygroups
baygrouped_model = mrt.model_randomtree(site = 2, maxdate = 20141018, mindate = 20140701,baygroup = bays)
baygrouped_model.change_from_default(max_depth = 50, max_features = 7, n_estimators = 50)
baygrouped_model.create_entries()
baygrouped_model.build()



for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	results_list.append(baygrouped_model.regress(snap))

del baygrouped_model

# build model without baygroups
standard_model = mrt.model_randomtree(site = 2, maxdate = 20141018, mindate = 20140701)
standard_model.change_from_default(max_depth = 50, max_features = 7, n_estimators = 50)
standard_model.create_entries()
standard_model.build()

for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	results_list.append(standard_model.regress(snap))
