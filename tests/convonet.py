import baygroup
import model_convonet as convo
import os
import snapshot as ss
import cPickle
	

# build model without baygroups
datafilename = './vectordb/convonet_data.pkl'
entryfilename = './vectordb/convonet_data_entries.pkl'
model = convo.model_convonet(site = 2, maxdate = 20141018, mindate = 20140701,n_samples = 2)

if not (os.path.isfile(entryfilename)):
	print "generating"
	model.create_entries()
	cPickle.dump(model.entries,open(entryfilename,'w'))
else:
	print "loading data"
	entries = cPickle.load(open(entryfilename,'r'))
	model.entries = entries

if not (os.path.isfile(datafilename)):
	print "generating vectors"
	data =  model.create_vector_package()
	cPickle.dump(data,open(datafilename,'w'))
else:
	print "loading data"
	results = cPickle.load(open(datafilename,'r'))


'''
for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	results_list.append(model.regress(snap))
'''