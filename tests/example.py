import sys
sys.path.append('../pautils/')
print sys.path
from pautils.baygroup import baygroup
import model_randomtree as mrt
import model_rawsvm as rawsvm
import snapshot as ss

# this is an example of how to create a model and regress a snapshot using the machine learning tool

# optional, first create a baygroup
# this is the name of a directory in ./baygroups/<baygroupname>
# in this case <baygroupname> is Stratford_missing
# for each text file in the directory, a baygroup will be made of the macs

bays = baygroup.baygroup('Stratford')

# now create a model
# this will create a model from the dabatase, using site = 2 (stratford), and using data between mindate and max data
# you can also set the number of feature vectors to use, default = 10000
# > rawsvm_model = rawsvm.model_rawsvm(site = 2, maxdate = 20141018, mindate = 20140701, samples = 5000)
# you can also use the baygroup to create multiple models for a site
# > rawsvm_model = rawsvm.model_rawsvm(site = 2, maxdate = 20141018, mindate = 20140701, baygroup = bays)

rawsvm_model = rawsvm.model_rawsvm(site = 2, maxdate = 20141018, mindate = 20140701)

# create entries will pull the data from the local database
rawsvm_model.create_entries()

# build will build a model
rawsvm_model.build()

	
# now do a regression
# each snapshot in the database is keyed from a date and a time
# here we are going to regress over six snapshots

snapshot_list = [('20141018','1500'),('20141018','1600'),('20141018','0900'),('20141017','1600'),('20141017','1400'),('20141017','0700')]

# for each snapshot we are going to grab the snapshot, and regress each snapshot, using the models .regress(snapshot) function.

results_list = []
for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	results_list.append(rawsvm_model.regress(snap))

# easily print out the total bays and the correct bays from this
totalbays = sum([d['total'] for d in results_list])
correctbay = sum([d['correct'] for d in results_list])
print correctbay/float(totalbays)
