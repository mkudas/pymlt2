import os
import sys 
sys.path.append('..')

import pamodels.rawsvm as rawsvm
import pautils.snapshot as ss
import cPickle

results_list = []
#bays = baygroup.baygroup('Stratford_missing30')
#snapshot_list = [('20141018','1500'),('20141018','1600'),('20141018','0900'),('20141017','1600'),('20141017','1400'),('20141017','0700')]
snapshot_list = [('20141018','1500')]

hogsvmfilename = './vectordb/hogsvm.pkl'

hogsvm_model = rawsvm.model_hogsvm(site = 9, maxdate = 20150501, mindate = 20150101, n_samples = 200)

results = hogsvm_model.create_entries()

hogsvm_model.load_results(results)
hogsvm_model.build()
predictions = hogsvm_model.test_regress()


'''
for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	results_list.append(hogsvm_model.regress(snap))

totalbays = sum([d['total'] for d in results_list])
correctbay = sum([d['correct'] for d in results_list])
print correctbay/float(totalbays)
'''

