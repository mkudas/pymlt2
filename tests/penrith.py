import os
import baygroup
import model_randomtree as mrt
import snapshot as ss
import cPickle

results_list = []
#bays = baygroup.baygroup('Stratford_missing30')

snapshot_list = [('20141202','0900')]

resultsfilename = './vectordb/penrith_20141203.pkl'

hogsvm_model = rawsvm.model_hogsvm(site = 66, maxdate = 20141203, mindate = 20141101, n_samples = 10000)

# build model without baygroups
if not (os.path.isfile(resultsfilename)):
	print "generating"
	results = hogsvm_model.create_entries()
	cPickle.dump(results,open(resultsfilename,'w'))
else:
	print "loading data"
	results = cPickle.load(open(resultsfilename,'r'))

hogsvm_model.load_results(results)
hogsvm_model.build()
predictions = hogsvm_model.test_regress()


'''
for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	results_list.append(hogsvm_model.regress(snap))

totalbays = sum([d['total'] for d in results_list])
correctbay = sum([d['correct'] for d in results_list])
print correctbay/float(totalbays)
'''

