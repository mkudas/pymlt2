import baygroup
import model_randomtree as mrt
import model_rawsvm as rawsvm
import snapshot as ss
	
results_list = []
bays = baygroup.baygroup('Stratford_missing30')
snapshot_list = [('20141018','1500'),('20141018','1600'),('20141018','0900'),('20141017','1600'),('20141017','1400'),('20141017','0700')]

# build model without baygroups
rawsvm_model = rawsvm.model_rawsvm(site = 2, maxdate = 20141018, mindate = 20140701)
rawsvm_model.create_entries()
rawsvm_model.build()

for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	results_list.append(rawsvm_model.regress(snap))

totalbays = sum([d['total'] for d in results_list])
correctbay = sum([d['correct'] for d in results_list])
print correctbay/float(totalbays)
