import model_randomtree as mrt
import baygroup
import snapshot as ss

site = 'Stratford'
snapshot_list = [('20141018','1500')]

def format_print_results(results):
	for result_key in results.keys():
		print "{0} : {1} : {2}".format(site,str(result_key), str(results[result_key]))

'''
# create standard model, print results
results_list = []
print "init model"
standard_model = mrt.model_randomtree(site = 2, maxdate = 20141018, mindate = 20141001)
print "creating..."
standard_model.create_entries()
print "building..."
standard_model.build()
for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])
	print "regressing..."
	results_list.append(standard_model.regress(snap))
for result in results_list:
	standard_model.print_regression(result)
'''


# create baygrouped models
bays = baygroup.baygroup('Stratford')

results_list = []
print "init model"
standard_model = mrt.model_randomtree(site = 2, maxdate = 20141018, mindate = 20141001,baygroup = bays)
print "creating..."
standard_model.create_entries()
print "building..."
standard_model.build()
for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])	
	print "regressing..."
	results_list.append(standard_model.regress(snap))


# create salted model

results_list = []
print "init model"
salted_model = mrt.model_saltedtree(site = 2, maxdate = 20141018, mindate = 20141001,baygroup = bays)
print "creating..."
salted_model.create_entries()
print "building..."
salted_model.build()
for snapshot in snapshot_list:
	snap = ss.snapshot(site = 2, datekey = snapshot[0], timekey = snapshot[1])	
	print "regressing..."
	results_list.append(salted_model.regress(snap))




