import paimage
import cv2
import skimage
import model_cvhog as svm

from sklearn.ensemble import RandomForestClassifier



mm = svm.model_opencvhog_zz(site = 2, maxdate = 20141018, mindate = 20140701, n_samples = 10000)

# get vectors from images
results = mm.create_entries()
mm.build()
mm.model = mm.init_model()

mm.train_model(mm.model,mm.vectors,mm.results)
mm.rtmodel = RandomForestClassifier(n_estimators = 50, max_depth = 50, max_features = 7)
mm.rtmodel.fit(mm.rtvectors,mm.results)

mm.regress()

#svm without 64
#{'totalveh': 3438, 'unpredicted': 0, 'totalbay': 4107, 'missed bay': 430, 'missed vehicle': 12, 'total': 7545, 'correct': 7103}

#rt 64
#{'totalveh': 0, 'unpredicted': 0, 'totalbay': 0, 'missed bay': 518, 'missed vehicle': 14, 'total': 0, 'correct': 7013}

# svm with 64 scaling
# {'totalveh': 3438, 'unpredicted': 0, 'totalbay': 4107, 'missed bay': 188, 'missed vehicle': 8, 'total': 7545, 'correct': 7349}


